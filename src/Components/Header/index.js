import React from "react";
import { withTheme } from "styled-components";
import styled from "styled-components/native";
import Header from "./Header";

export default styled(
  withTheme(props => {
    const style = {
      ...props.style,
      header: {
        height: 64,
        backgroundColor:'#465066'
      },
      left: {
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center"
      },
      middle: {
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center"
      },
      right: {
        backgroundColor: "transparent"
      }
    };
    return <Header {...props} style={style} />;
  })
)``;

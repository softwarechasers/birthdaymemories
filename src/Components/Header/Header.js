import React, { Component } from "react";
import { Col, Row, Grid } from "react-native-easy-grid";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";
import { View, Text } from "react-native";
import LinearGradient from 'react-native-linear-gradient';

export default class Header extends Component {
  static propTypes = {
    style: PropTypes.object.isRequired,
    handleMenu: PropTypes.func,
    title:PropTypes.string
  };
  static defaultProps = {
    handleMenu: f => f
  };
  render() {
    const { style } = this.props;
    return (
      <View style={style.header}>
        <LinearGradient 
        //start={{x: 0.05, y: 0.1}} end={{x: 0.05, y: 1.0}}
        //locations={[0.1,0.5,0.6]}
        colors={['#53b6ba','#5151E5','#5151E9']} style={style.header}>
        <Grid>
          <Row>
            <Col size={20} style={style.left}>
              <Icon
                type="FontAwesome"
                name="ios-menu"
                style={{
                  color: "white",
                  fontSize: 26
                }}
                onPress={this.props.handleMenu}
              />
            </Col>
            <Col size={60} style={style.middle}>
              <Text style={{color:"white",fontSize: 21,fontFamily:"GoogleSans-Medium"}}>{this.props.title}</Text>
            </Col>
            <Col size={20} style={style.right} />
          </Row>
        </Grid>
        </LinearGradient>
      </View>
    );
  }
}

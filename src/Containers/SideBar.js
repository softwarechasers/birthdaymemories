import React, { Component } from 'react';
import {
  Text,
  Dimensions,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  ImageBackground
} from 'react-native';
import { Icon } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';

export default class SideBar extends Component {
  constructor() {
    super();
    this.state = {
      menu: [{
        Icon:'image',
        IconType:'FontAwesome',
        Item:'My Memories'
      },
      {
        Icon:'group',
        IconType:'FontAwesome',
        Item:'BirthDay Wishes'
      },{
        Icon:'collections',
        IconType:'MaterialIcons',
        Item:'My Birthday Collection'
      }],
      
    };
  }

  onSelectedItem(menu) {
    //alert(JSON.stringify(menu.Item))
     if (menu.Item == 'My Memories') {
      this.props.onCloseMenu('My Memories');
     } else if (menu.Item == 'My Birthday Collection') {
      this.props.onCloseMenu('My Birthday Collection');
     } else {
      this.props.onCloseMenu('BirthDay Wishes');
    }
  }

  renderBasic({ item, index }) {
    return (
      <View style={{flex: 1}}>
        
        <TouchableOpacity onPress={this.onSelectedItem.bind(this, item)}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent:'space-around',
              marginBottom: 6,
              marginVertical: 0,
              backgroundColor:'transparent',
              marginBottom:10
            }}
          >
          <View style={{flex:0.16,alignItems:'flex-end'}}>
          <Icon type={item.IconType} name={item.Icon} style={{fontSize:19,marginRight:15,color:'white'}}/>
          </View>
          <View style={{flex:0.84}}>
          <Text  style={{color:'white',fontSize:19,marginLeft:6,fontFamily:'GoogleSans-Regular'}}>{item.Item}</Text>
          </View>
          
          </View>
        </TouchableOpacity>
        
      </View>
    );
  }

  render() {
    return (
      <View
        style={{
          backgroundColor: '#465066',
          height: SCREEN_HEIGHT
        }}
      >
         
        <LinearGradient 
        start={{x: 0.1, y: 0.1}} end={{x: 0.3, y: 1.0}}
        //locations={[0.1,0.5,0.6]}
        colors={['#72EDF2','#5151E5']} style={{flex: 1}}>
      <View style={{flex:0.25,justifyContent:'center',alignItems:'center'}}>
      <TouchableOpacity>
          <View
            style={{
              width: SCREEN_WIDTH / 3,
              height: SCREEN_WIDTH / 3,
              borderRadius: SCREEN_WIDTH / 6,
              backgroundColor: '#f2f2f2',
              justifyContent: 'center',
              marginTop: SCREEN_WIDTH / 10,justifyContent:'center'
            }}
          >
            <Image
              source={require('@images/profile.jpg')}
              style={{
                width: SCREEN_WIDTH / 2,
                height: SCREEN_WIDTH / 2,
                borderRadius: SCREEN_WIDTH / 3,
                alignSelf:'center'
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View style={{flex:0.75}}>
      <FlatList
          style={{marginTop:60}}
          data={this.state.menu}
          extraData={this.state}
          renderItem={this.renderBasic.bind(this)}
        />
      </View>
      </LinearGradient>
      </View>
    );
  }
}

// function mapStateToProps(state) {
//   return {
//     menu: state.menu
//   };
// }
// function mapDispatchToProps(dispatch) {
//   return {
//     _changeMenu: () => dispatch(changeMenu())
//   };
// }
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(SideBarContent);

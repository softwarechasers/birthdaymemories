import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  AppState,
  StatusBar,
  Dimensions,
  Image
} from "react-native";
import { Drawer } from "native-base";
import Header from "@components/Header/index.js";
import SideBar from "@containers/SideBar";
import Animation from "lottie-react-native";
const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;
import cpLoader from "@animations/balloons_animation.json";
import { StackActions, withNavigation } from "react-navigation";
var Sound = require("react-native-sound");

class Splash extends Component {
  componentWillMount() {
    const { params } = this.props.navigation.state;
    setTimeout(() => {
        const pushAction = StackActions.push({
            routeName: 'Main',
            params: {
            }
          });
          this.props.navigation.dispatch(pushAction);  
    }, 6000);
  }
  componentDidMount() {
    const callback = (error, sound) => {
      if (error) {
        Alert.alert("error", error.message);
        //   setTestState(testInfo, component, 'fail');
        return;
      }
      sound.play(() => {
        sound.release();
      });
      //sound.setNumberOfLoops(-1);
    };
    let sound = new Sound(require("./birthday.mp3"), error =>
      callback(error, sound)
    );
    AppState.addEventListener("change", state => {
      if (state === "background") {
        sound.stop();
      }
    });
  }
  // componentWillUnmount() {
  //   AppState.removeEventListener("change", this._handleAppStateChange);
  // }
  // _handleAppStateChange(currentAppState) {
  //   if (currentAppState == "background") {
  //     // alert("sound");
  //     Sound.stop();
  //   }
  //   if (currentAppState == "active") {
  //     resume();
  //   }
  // }
  render() {
    return (
      <View>
        <StatusBar hidden={true} />
        <ImageBackground
          source={require("@images/splash1.jpeg")}
          style={{ width: SCREEN_WIDTH, height: SCREEN_HEIGHT }}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: "transparent",
              flexDirection: "column"
            }}
          >
            <View style={{ flex: 7, backgroundColor: "transparent" }} />
            <View
              style={{
                flex: 3,
                backgroundColor: "transparent",
                flexDirection: "row"
              }}
            >
              <Image
                source={require("@images/ballonwish.gif")}
                style={{ height: 200, width: SCREEN_WIDTH }}
              />
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});

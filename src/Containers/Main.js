import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  StatusBar,
  Dimensions,
  Image
} from "react-native";
import { Drawer } from "native-base";
import Header from "@components/Header/index.js";
import SideBar from "@containers/SideBar";
import Animation from "lottie-react-native";
const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;
import * as Animatable from 'react-native-animatable';
import Gallery from 'react-native-image-gallery';
import LinearGradient from 'react-native-linear-gradient';
import TextTicker from 'react-native-text-ticker'

class Main extends Component {
  constructor(props){
    super(props);
    this.state={
      screen:'BirthDay Wishes'
    }
  }
  handleMenu() {
    this.drawer._root.open();
  }
  closeDrawer(params) {
    //alert('closed' + params);
    if (params === 'My Memories') {
      this.setState({
        screen: 'My Memories',
      });
    } else if (params === 'BirthDay Wishes') {
      //alert('closed' + params);
      this.setState({
        screen: 'BirthDay Wishes',
      });
    } else if (params === 'My Birthday Collection') {
      this.setState({
        screen: 'My Birthday Collection',
      });
    }

    this.drawer._root.close();
  }

  render() {
    return (
      <Drawer
        ref={ref => {
          this.drawer = ref;
        }}
        content={
          <SideBar
            navigation={this.props.navigation}
            onCloseMenu={params => this.closeDrawer(params)}
          />
        }
        onClose={() => this.closeDrawer()}
      >
        <View style={styles.container}>
          <StatusBar backgroundColor="#53b6ba" barStyle="light-content" />
          <Header handleMenu={this.handleMenu.bind(this)} title={this.state.screen}/>
          {this.state.screen == 'BirthDay Wishes'?
          (<ImageBackground
            source={{
              uri:
                "https://i.pinimg.com/originals/c7/b0/53/c7b053c342fbd2611e174415e1cd9e47.gif"
            }}
            style={{ flex:1,width: SCREEN_WIDTH, height: SCREEN_HEIGHT,alignItems:'center' }}
          >
            <View style={{flex:0.9,marginTop:SCREEN_HEIGHT/3.6}}>          
            <Animatable.Text animation="zoomIn"  iterationCount="infinite" style={{ textAlign: 'center',fontFamily:'GoogleSans-Medium',fontSize:31,marginBottom:30 ,color:'#fcc900' }}>Happy Birth Day</Animatable.Text>
            <Animatable.Text animation="zoomIn"  iterationCount="infinite" style={{ textAlign: 'center',fontFamily:'GoogleSans-Medium',fontSize:26,marginBottom:30 ,color:'#fcc900' }}>To</Animatable.Text>
            <Animatable.Text animation="zoomIn"  iterationCount="infinite" style={{ textAlign: 'center',fontFamily:'GoogleSans-Medium',fontSize:42,marginBottom:0 ,color:'#fcc900' }}>MAGGIE</Animatable.Text>
            </View>
        <View style={{flex:0.1}}>
        <TextTicker
          style={{ fontSize: 16,color:'#00effc',fontFamily:'GoogleSans-MediumItalic',alignSelf:'flex-end'}}
          
          loop
          repeatSpacer={50}
          marqueeDelay={2000}
        >
          You are known for always giving of yourself. It makes you even more special 😎🎂🍼🍫🤗😊.
        </TextTicker>
        </View>
            </ImageBackground>)
          :this.state.screen == 'My Memories'?
          (<View style={{ flex: 1, backgroundColor: 'black' }}>
            <Gallery
        style={{ flex: 1, backgroundColor: 'black' }}
        images={[
          { source: require('@images/memories/pic1.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic2.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic3.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic4.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic5.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic6.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic7.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic8.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic9.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic10.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic11.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic12.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic13.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic14.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic15.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic16.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic17.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic18.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic19.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic20.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic21.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic22.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic23.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic24.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic25.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic26.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic27.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/memories/pic28.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
        ]}
      />
            </View>)
          :(<View style={{ flex: 1, backgroundColor: 'black' }}>
       <Gallery
        style={{ flex: 1, backgroundColor: 'black' }}
        images={[
          { source: require('@images/previousBirthday/pic1.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic2.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic3.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic4.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic5.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic6.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic7.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic8.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic9.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },
          { source: require('@images/previousBirthday/pic10.jpeg'), dimensions: { width: SCREEN_WIDTH, height: 150 } },

        ]}
      />
            </View>)}
        </View>
      </Drawer>
    );
  }
}

export default Main;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});


import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Main from '@containers/Main.js';
import Splash from '@containers/Splash.js';
// import ExploreBrazilian from '@containers/ExploreBrazilian.js';
// import ExploreFoxSports from '@containers/ExploreFoxSports.js';
// import MoreAbout from '@containers/MoreAbout.js';
// import sample from '@containers/sample.js';

const StackNaviagtion = createStackNavigator(
  {
    Main:{screen :Main},
    Splash:{screen:Splash}
  },
  {
  initialRouteName:'Splash',
  headerMode:'none',
  navigationOptions: {
    gesturesEnabled: false
  }
  }
);

const AppNaviagtion = createAppContainer(StackNaviagtion)
export default AppNaviagtion



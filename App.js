import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { ThemeProvider } from "styled-components";
import Main from "./src/App";

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ThemeProvider theme={{}}>
          <Main />
        </ThemeProvider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
